package main

import (
	"sync"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
)

var (
	db, _          = gorm.Open("sqlite3", "./gorm.db") // коннект к бд
	sourceFileChan = make(chan CurrentFile)            // основной канал для передачи того что надо конверитить
	Conf           = new(Config)                       // сингтон в котором наодится все что взяли в jsone
	CheckedMap     = make(map[int]CheckedConfig)       // мапа выжывших после првоерки пресетов)
	wg             sync.WaitGroup
)
