package main

import (
	"github.com/jinzhu/gorm"
)

type SourceFile struct {
	gorm.Model
	Path      string
	PresetID  uint
	Converted bool
}

type TargetFile struct {
	gorm.Model
	Path         string
	PresetID     uint
	SourceFileID uint
}

type Preset struct {
	gorm.Model
	Humanid      int
	TargetFile   []TargetFile
	SourceFile   []SourceFile
	Args         string
	SourceExt    string
	Ext          string
	FFmpegtreads int
	Deltime      int
}

type ErrorFile struct {
	gorm.Model
	SourceFile   SourceFile
	SourceFileID uint
	Message      string
	ErrorCode    string
}

// Модели для заполнения парсером!
type Config struct {
	Global  Global
	Configs []Configs
}

// Отдельные секции конфигов

type Configs struct {
	Source string
	Target string
	Preset int
}

// Global глобальная секция которая заполняется из json конфига
type Global struct {
	Convertor int
	Webserver bool
}

// CheckedConfig сюда заполняются выжывшие после проверки конфиги
type CheckedConfig struct {
	Configs    Configs // конфиг
	FindPreset Preset  // пресет к этому конфигу найденный в базе
}

// CurrentFile эта структура содержит файл для коневртации + CheckedConfig для этого файла, эта структура у каждого фала своя и путеществует по всем каналам, и каждая
// фунция  делает какие то действтя на основе кокртеного экземляра струтуры
type CurrentFile struct {
	CheckedConfig  CheckedConfig
	SourceFile     SourceFile
	TargetFile     TargetFile
	ErrorFile      ErrorFile
	WithoutExtFile string
}
