package main

import (
	"fmt"
	"os"
	"path/filepath"
	"time"
)

func listFunc(mapNumber int) filepath.WalkFunc {
	return func(path string, file os.FileInfo, err error) error {
		CurrentFileStructure := new(CurrentFile)
		CurrentFileStructure.CheckedConfig = CheckedMap[mapNumber]
		if filepath.Ext(path) == CurrentFileStructure.CheckedConfig.FindPreset.SourceExt {
			source := SourceFile{}
			db.Where("Path = ?", path).First(&source)
			if source.ID == 0 {
				fmt.Println("Запись для файла " + path + " отсуствует")
				fmt.Println(path)
				file := SourceFile{Path: path, PresetID: CurrentFileStructure.CheckedConfig.FindPreset.ID, Converted: false}
				db.Create(&file)
				time.Sleep(3 * time.Second)
				CurrentFileStructure.SourceFile = file
				go filewatcher(*CurrentFileStructure, 60, 30)

			}
		}
		return nil
	}
}
