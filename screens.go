package main

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
)

func CreateScreen(conv CurrentFile) {
	screen_dir := conv.CheckedConfig.Configs.Target + "screens/"
	_, err := os.Stat(screen_dir)
	if err != nil {
		erre := os.Mkdir(screen_dir, 0777)
		if erre != nil {
			fmt.Println(erre)
			return
		}
	}
	for i := 1; i < 5; i++ {
		parametrs := "ffmpeg -y -ss 00:00:" + strconv.Itoa(i) + "0.000 -i " + conv.TargetFile.Path + " -s 854x480 -aspect 16:9 -vframes 1 " + screen_dir + conv.WithoutExtFile + "_" + strconv.Itoa(i) + ".jpeg"

		o, err := exec.Command("bash", "-c", parametrs).CombinedOutput()
		if err != nil {
			fmt.Print(string(o))
		}

	}

}
