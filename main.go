// presets
// 1 ( mov to mp4)
// -strict experimental -c:a aac -b:a 384k -ar 48000 -vcodec libx264 -pix_fmt yuv422p -preset veryslow -crf 16 -coder 1 -flags +loop -cmp chroma -partitions +parti4x4+partp8x8+partb8x8 -me_method hex -subq 6 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -b_strategy 1

// ============================================================================
// Конфиги заполняются по образцу

//  {
//             "source": "/home/nextel/test1/",     -  откуда берем , закрывающий слеш обязателен
//             "target": "/home/nextel/test1/conv/",   -  куда кладем, закрывающий слеш обязателен
//             "preset": 1 - пресет
//         } -между всеми блоками должна быть запятая,  в случае если блок не последний

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func main() {
	// Создаем бд, чекаем таблицы
	db.DB()
	defer db.Close()
	db.CreateTable(&Preset{})
	db.CreateTable(&SourceFile{})
	db.CreateTable(&TargetFile{})
	db.CreateTable(&ErrorFile{})

	// Чекаем файл  конфига
	file, err := ioutil.ReadFile("./config.json")
	if err != nil {
		fmt.Println("файл не хочет читаться)", err.Error())
		panic("FIRE!!! FILE")
	}
	// пытаемся мапить  json  в глобальную структуру (gloablvars.go)
	if err := json.Unmarshal(file, &Conf); err != nil {
		fmt.Println("Не валидный json", err.Error())
		panic("FIRE!!! JSON")
	}
	// удаляем все что не сконвертилось к хуям собачим!

	db.Where("Converted LIKE ?", false).Delete(SourceFile{})

	//проходимся по конфигам  и вызываем на каждом элементе три проверки
	// 1. есть ли сорс 2. есть ли таргет 3. есть ли пресет указанный в струтре
	for key, value := range Conf.Configs {
		fmt.Println("ключ ", key)
		check_configs(value, key)
	}

	// Обходим мапу проверяем чо записалось
	println("чекаем мапу")
	for key, value := range CheckedMap {
		fmt.Println("ключ ", key, "Значснеие ", value)
	}
	println("Закночили чекатьмапу мапу")

	// берем из глобала количество конверторов и замускаем их в цикле!
	NUM_WORKERS := Conf.Global.Convertor
	for i := 1; i <= NUM_WORKERS; i++ {
		go convert()
	}

	// обходим мапу с уцелевшими  после проверки массивами и замускаем вотчер для каждой!

	for key, _ := range CheckedMap {
		wg.Add(1)
		fmt.Println("Добавили вг ", key)
		go DirecoryWatcher(key)
	}
	wg.Wait()

}

// for index := 0;  < 4; index++ {
//     go func(){
//       	for {
//        filepath.Walk(source_argv, listFunc) // сорс разный во всех случаях
// 		time.Sleep(3 * time.Second)
//  	}
//     }

// 	for {

// 		time.Sleep(3 * time.Second)
// 	}

// }

// образец вызова мисин функйции

// package main

// import (
// 	"fmt"
// 	"os"
// 	"path/filepath"
// )

// type second func(a, b, c int) error

// func main() {
// 	filepath.Walk("/home/nextel/", listFunc("sddsd"))

// }

// func listFunc(b string) filepath.WalkFunc {
// 	return func(v string, f os.FileInfo, err error) error {
// 		fmt.Println(b + " " + v)
// 		return nil
// 	}

// }
