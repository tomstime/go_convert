package main

import (
	"fmt"
	"os"
	"os/exec"
)

func CheckResultFile(conv CurrentFile) {
	parametrs := "ffmpeg -v error -i " + conv.TargetFile.Path + " -f null -  "
	fmt.Println("parm ", parametrs)
	o, err := exec.Command("bash", "-c", parametrs).CombinedOutput()
	if err != nil {
		passfile := ErrorFile{SourceFile: conv.SourceFile, ErrorCode: err.Error(), Message: string(o), SourceFileID: conv.SourceFile.ID}
		db.Create(&passfile)
		fmt.Println("Коневртация завершилась с ошибкой ", passfile.SourceFile)
		return
	}

	if string(o) != "" {
		db.Delete(&conv.TargetFile)
		db.Delete(&conv.SourceFile)
		os.Remove(conv.TargetFile.Path)
	} else {

		conv.SourceFile.Converted = true
		db.Save(&conv.SourceFile)
		CreateScreen(conv)
	}
}
