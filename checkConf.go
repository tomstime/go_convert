package main

import (
	"bufio"
	"fmt"
	"os"
)

func check_configs(currentConfigArray Configs, place int) {
	if _, err := os.Stat(currentConfigArray.Source); os.IsNotExist(err) {
		println("  source path", currentConfigArray.Source, " not exist")
	} else {
		if _, err := os.Stat(currentConfigArray.Target); os.IsNotExist(err) {
			println("target path not exist create it?")
			var target_path string
			fmt.Scan(&target_path)
			if target_path == "y" {
				erre := os.Mkdir(currentConfigArray.Target, 0777)
				if erre != nil {
					fmt.Println(erre)
					return
				}
			} else {
				fmt.Println("Bye!")
				return
			}

		}
		pr := Check_presets(currentConfigArray)
		currentstrut := CheckedConfig{Configs: currentConfigArray, FindPreset: pr}
		CheckedMap[place] = currentstrut

	}

}

func Check_presets(currentConfigArray Configs) Preset {
	var new_preset string
	finded_preset := Preset{}
	db.Where("Humanid = ?", currentConfigArray.Preset).First(&finded_preset)
	if finded_preset.Humanid == 0 {
		println("cant find preset with ID", currentConfigArray.Preset, "create it?")
		fmt.Scanln(&new_preset)
		if new_preset == "y" {
			var sourceExt string
			var ext string
			var ffmeg int
			var args string
			var deleteday int
			fmt.Println("define extension of sourcefile")
			fmt.Scan(&sourceExt)
			fmt.Println("define extension of targetfile")
			fmt.Scan(&ext)
			fmt.Println("define number of ffmeg threads per file")
			fmt.Scan(&ffmeg)
			fmt.Println("define convert args")
			scanner := bufio.NewScanner(os.Stdin)
			for scanner.Scan() {
				args = scanner.Text()
				fmt.Println(args)
				break
			}
			fmt.Println("define how much days store source files before delete")
			fmt.Scan(&deleteday)

			pr := Preset{Args: args, Deltime: deleteday, Ext: ext, FFmpegtreads: ffmeg, Humanid: currentConfigArray.Preset, SourceExt: sourceExt}
			db.Create(&pr)
			finded_preset = pr
			fmt.Print(finded_preset.Humanid)
			return finded_preset
		} else {
			fmt.Println("BYE!")

		}
	}
	return finded_preset
}
